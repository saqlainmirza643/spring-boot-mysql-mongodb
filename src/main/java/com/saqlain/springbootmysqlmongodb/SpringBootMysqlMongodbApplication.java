package com.saqlain.springbootmysqlmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMysqlMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMysqlMongodbApplication.class, args);
	}

}
